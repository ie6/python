即使之前你从未写过1行代码，也能在学完本课程后，达到Python入门水平，能开发300-500行代码的小程序，掌握基本的编程思维、软件设计方法。无论你日后想做人工智能、数据分析，还是WEB开发、爬虫、大数据等，都应该先把这部分基础掌握

#### 项目介绍
各种开发案例，不定期更新。

| 软件 | 版本  | 功能|   地址|
| ---- | ----- |----- |----- |
|   Python   |  3.7.1 |  脚本语言   | https://www.python.org/  |
|   Django   | 2.1.3 |   Web框架|  https://www.djangoproject.com/ |
|   PyCharm| 2018.2.4 |  可视化开发工具| http://www.jetbrains.com/pycharm/  |

#### 专栏地址
- Python开发 https://www.jianshu.com/c/e420f3140cbf

#### 功能新增/修改记录
| 时间   |   更新记录 |   地址  |
| -- | -- | -- |
|2019年5月8日  | 模拟chrome浏览器                         |https://gitee.com/icloud-iot/python/tree/master/Day01    |
|2019年5月9日  | Python实现mysql数据库连接池              |https://gitee.com/icloud-iot/python/tree/master/Day02    |
|2019年5月10日 | python 抓取eureka 注册中心列表           |https://gitee.com/icloud-iot/python/tree/master/Day03    |
|2019年5月11日 | python 爬取网络小说并且入库              |https://gitee.com/icloud-iot/python/tree/master/Day04    |
|2019年5月12日 | python 字符处理                          |https://gitee.com/icloud-iot/python/tree/master/Day05    |
|2019年5月13日 | python操作数据库数据前端展示             |https://gitee.com/icloud-iot/python/tree/master/Day06   |
|2019年5月14日 | python 日志处理                          |https://gitee.com/icloud-iot/python/tree/master/Day07    |
|2019年5月16日 | python 微信机器人预警通知                |https://gitee.com/icloud-iot/python/tree/master/Day08    |
|2019年5月17日 | python操作openCV的库实现智能识别         |https://gitee.com/icloud-iot/python/tree/master/Day09    |
|2019年5月18日 | Python3+Django2配置后台管理              |https://gitee.com/icloud-iot/python/tree/master/Day10    |
|2019年5月19日 | Python IP代理池爬取                      |https://gitee.com/icloud-iot/python/tree/master/Day11    |
|2019年5月22日 | python 快代理IP代理池爬取                |https://gitee.com/icloud-iot/python/tree/master/Day12    |
|2019年5月23日 | python 爬取最新电影与短评，文件存储      |https://gitee.com/icloud-iot/python/tree/master/Day13    |
|2019年5月25日 | Python 语音文字转化与播报                |https://gitee.com/icloud-iot/python/tree/master/Day14    |
|2019年5月27日 | Python 微信网页端登录与自动回复          |https://gitee.com/icloud-iot/python/tree/master/Day15    |
|2019年5月28日 | Python 打造个人简约博客                  |https://gitee.com/icloud-iot/python/tree/master/Day16    |
|2019年5月30日 | Python 代理池IP代理池爬取重构            |https://gitee.com/icloud-iot/python/tree/master/Day17    |
|2019年6月1日  | Python numpy 处理                        |https://gitee.com/icloud-iot/python/tree/master/Day18    |
|2019年6月5日  | Python bpmn工作流处理                    |https://gitee.com/icloud-iot/python/tree/master/Day19    |
|2019年6月8日  | python 网络爬虫处理优化IP代理池入库管理  |https://gitee.com/icloud-iot/python/tree/master/Day20    |
|2019年6月10日 | Python 车牌定位与识别                    |https://gitee.com/icloud-iot/python/tree/master/Day21    |
|2019年6月11日 | python 打造智能闹钟                      |https://gitee.com/icloud-iot/python/tree/master/Day22    |
|2019年6月12日 | Python 人脸识别并微信推送识别结果        |https://gitee.com/icloud-iot/python/tree/master/Day23    |
|2019年6月13日 | Python 字符/路径/循环空间For各种处理方式  |https://gitee.com/icloud-iot/python/tree/master/Day25|  | |
|2019年6月14日 | Python 依赖包关系|Excel|集合空间|文件操作处理  |https://gitee.com/icloud-iot/python/tree/master/Day00|  | |

####  欢迎关注"码农架构"微信公众号，将不定期更新，专注于系统架构、高可用、高性能、高并发类技术分享
![输入图片说明](https://images.gitee.com/uploads/images/2019/0615/033444_48cf25d3_1468963.jpeg "微信公众号宣传副本.jpg")

##  淘宝镜像

https://npm.taobao.org/

## 参考学习

https://www.python.org/

https://docs.djangoproject.com/

http://www.jetbrains.com/pycharm/

http://www.runoob.com/python3/python3-tutorial.html
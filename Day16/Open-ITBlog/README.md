# my_blog
![](https://img.shields.io/badge/python-3.7-brightgreen.svg) ![](https://img.shields.io/badge/django-2.1-ff69b4.svg) ![](https://img.shields.io/badge/Powered%20by-%40%20opsonly-blue.svg)
 > 基于```python3.7```和```django2.1```的多人博客系统
---

## 简介
> 该博客前段框架使用了Bootstrap 4，在其基础上添加了一些自己需要用的css样式，后端使用django2.1。
> 由于自己也在摸索中，会继续完善此项目的功能。觉得有用的欢迎给个star！~

## 主要功能:
---
- 用户注册，登录,删除，以及使用第三方库password_reset来重置用户密码
- 文章的发布，修改以及删除，支持markdown以及代码高亮
- 留言板系统
- 支持文章的多人互动评论
- 文章标签功能，通过标签搜索相关文章

## 环境
```python
python3.7
```


## 问题相关
- 关注微信公众号``码农架构`` 后留言
- 阿里工作比较忙，个人晚上才会去看微信公众号，不便之处见谅

## 版本更新地址
``` https://gitee.com/icloud-iot/iOpen-ITBlog ```